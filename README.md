# Climbing-Gym
A website for a local climbing gym. This website will hold different information related to the gym itself, the climbing routes it has and the events that will take place in it. users will be able to reserve  time slots and participate in different competitions that the gym is holding. Employees will deal with every user interaction, and the events the gym will be holding. Admins will be dealing with the employees. 

## Build instructions
1. Make sure javaJDK13 is downaloded in your system
2. The project uses gradle so that should be configured into your system too
3. When wanting to try some HTTP requests (Problem controller is the only working one currently) use postman to check the requests 

## Link to Jira (Backlog)
I would need your email to send you the link to the jira backlog
