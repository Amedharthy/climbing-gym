package com.gymapp.app.ws;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.gymapp.app.ws.model.Event;
import com.gymapp.app.ws.model.Problem;

import com.gymapp.app.ws.repository.EventRepository;
import com.gymapp.app.ws.repository.ProblemRepository;
import com.gymapp.app.ws.services.ProblemService;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
class EventIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EventRepository repository;


    @Test
    public void EventCreation() throws Exception {
        Event event = new Event("event", "description", 4, "Date",4);
        ObjectWriter eventO = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String content = eventO.writeValueAsString(event);

        this.mockMvc.perform(post("/event")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isCreated())
                .andReturn();
        this.mockMvc.perform(get("/event"))
                .andExpect(status().isOk())
                .andReturn();
    }

}
