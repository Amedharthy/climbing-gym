package com.gymapp.app.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.gymapp.app.ws.model.Problem;

import com.gymapp.app.ws.repository.ProblemRepository;
import com.gymapp.app.ws.services.ProblemService;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
class ProblemIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProblemRepository repository;

    @MockBean
    private ProblemService service;

    @Test
    public void ProblemCreation() throws Exception {
        Problem problem = new Problem("V6",2);
        ObjectWriter prob = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String content = prob.writeValueAsString(problem);

        this.mockMvc.perform(post("/problem")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isCreated())
                .andReturn();
        this.mockMvc.perform(get("/problem"))
                .andExpect(status().isOk())
                .andReturn();
    }

}
