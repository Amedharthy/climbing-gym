package com.gymapp.app.ws.services;

import com.gymapp.app.ws.model.Account;
import com.gymapp.app.ws.model.Event;
import com.gymapp.app.ws.model.request.AccountCreateRequest;
import com.gymapp.app.ws.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService  {

    @Autowired
    AccountRepository accountRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public Account readAccountByUsername (String userName) {
        return accountRepository.findByUserName(userName).orElseThrow(EntityNotFoundException::new);
    }

    public List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();
        accountRepository.findAll().forEach(accounts::add);
        return accounts;
    }


    public void createAccount(AccountCreateRequest accountCreateRequest) {
        Account account = new Account();

        Optional<Account> byUsername = accountRepository.findByUserName(accountCreateRequest.getUsername());

        if (byUsername.isPresent()) {
            throw new RuntimeException("User already registered. Please use different username.");
        }

        account.setUserName(accountCreateRequest.getUsername());
        account.setPassword(passwordEncoder.encode(accountCreateRequest.getPassword()));
        account.setRole(accountCreateRequest.getRole());
        accountRepository.save(account);
    }

}
