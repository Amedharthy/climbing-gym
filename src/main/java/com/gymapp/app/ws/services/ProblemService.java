package com.gymapp.app.ws.services;

import com.gymapp.app.ws.model.Problem;
import com.gymapp.app.ws.repository.ProblemRepository;
import com.gymapp.app.ws.serviceInterface.ProblemServiceInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProblemService implements ProblemServiceInterface {

    private ProblemRepository problemRepository;

    public ProblemService(ProblemRepository problemRepository) {
        this.problemRepository = problemRepository;
    }

    @Override
    public List<Problem> getProblems() {
        List<Problem> problems = new ArrayList<>();
    problemRepository.findAll().forEach(problems::add);
    return problems;
    }

    @Override
    public Problem getProblemById(Long id) {
        return problemRepository.findById(id).orElse(null);
    }

    @Override
    public Problem addProblem(Problem problem) {
        return problemRepository.save(problem);
    }

    @Override
    public void updateProblem(Long id, Problem problem) {
        Problem problemUp = problemRepository.findById(id).get();
        System.out.println(problemUp.toString());
        problemUp.setDifficulty(problem.getDifficulty());
        problemUp.setFloor(problem.getFloor());
        problemRepository.save(problemUp);
    }

    @Override
    public void deleteProblem(Long id) {
        problemRepository.deleteById(id);
    }
}
