package com.gymapp.app.ws.services;

import com.gymapp.app.ws.model.Event;
import com.gymapp.app.ws.repository.EventRepository;
import com.gymapp.app.ws.serviceInterface.EventServiceInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService implements EventServiceInterface {

    private EventRepository eventRepository;

    public EventService(EventRepository eventRepository) { this.eventRepository = eventRepository;}

    @Override
    public List<Event> getEvents() {
        List<Event> events = new ArrayList<>();
        eventRepository.findAll().forEach(events::add);
        return events;
    }

    @Override
    public Event getEventById(Long id) {
        return eventRepository.findById(id).get();
    }

    @Override
    public Event addEvent(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public void updateEvent(Long id, Event problem) {
        Event events = eventRepository.findById(id).get();
        System.out.println(events.toString());
        events.setEventName(events.getEventName());
        events.setParticpantRange(events.getParticpantRange());
        events.setDescription(events.getDescription());
        events.setEventDate(events.getEventDate());
        events.setNrParticpants(events.getNrParticpants());
        eventRepository.save(events);
    }

    @Override
    public void deleteEvent(Long id) {
        eventRepository.deleteById(id);
    };

}
