package com.gymapp.app.ws.model;

public enum AccountType {
    Administrator,
    Employee,
    User
}
