package com.gymapp.app.ws.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
@Data
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String eventName;
    private String description;
    private int particpantRange;
    private String eventDate;
    private int nrParticpants;


    public Event(String eventName, String description, int particpantRange, String eventDate, int nrParticpants) {
        this.eventName = eventName;
        this.description = description;
        this.particpantRange = particpantRange;
        this.eventDate = eventDate;
        this.nrParticpants = nrParticpants;
    }

    public Event() {

    }

    public Long getEvid() {
        return id;
    }

    public String getEventName() { return eventName; }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getParticpantRange() { return particpantRange; }
    public void setParticpantRange(int particpantRange) { this.particpantRange = particpantRange; }

    public String getEventDate() { return eventDate; }
    public void setEventDate(String eventDate) { this.eventDate = eventDate; }

    public int getNrParticpants() { return nrParticpants; }
    public void setNrParticpants(int nrParticpants) { this.nrParticpants = nrParticpants; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
