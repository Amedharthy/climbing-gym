package com.gymapp.app.ws.model;

import java.util.Objects;

public class Admin {
    private int id;
    private String firstName;
    private String lastName;
    private String db;

    public Admin(int id, String firstName, String lastName, String db ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.db = db;
    }

    public Admin() {
    }

    public int getId() {
        return id;
    }
    public void setHoldId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName () {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return db;
    }
    public void setDateOfBirth(String db) {
        this.db = db;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Admin admin = (Admin) o;
        return id == admin.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
