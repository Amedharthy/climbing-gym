package com.gymapp.app.ws.model.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AccountCreateRequest {
    private String userName;
    private String password;
    private String role;

    public AccountCreateRequest(String userName, String password, String role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }
    public String getUsername() {
        return userName;
}
    public String getPassword() {
        return password;
    }
    public String getRole() {
        return role;
    }
}
