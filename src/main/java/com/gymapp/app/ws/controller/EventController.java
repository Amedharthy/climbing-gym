package com.gymapp.app.ws.controller;

import com.gymapp.app.ws.model.Event;
import com.gymapp.app.ws.model.Problem;
import com.gymapp.app.ws.serviceInterface.EventServiceInterface;
import com.gymapp.app.ws.serviceInterface.ProblemServiceInterface;
import com.gymapp.app.ws.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("event")
public class EventController {

    EventServiceInterface eventService;

    public EventController(EventServiceInterface eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public ResponseEntity<List<Event>> getProblems() {
        List<Event> events = eventService.getEvents();
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<Event> getEventById(@PathVariable Long id) {
        Event event = eventService.getEventById(id);

        if(event != null) {
            return new ResponseEntity<>(event, HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping
    public ResponseEntity<Event> saveProblem(@RequestBody Event eve) {
        Event event = eventService.addEvent(eve);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("event", "/event");
        return new ResponseEntity<>(event, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<Event> updateTodo(@PathVariable("id") Long id, @RequestBody Event eve) {
        Event event = eventService.getEventById(id);

        if(event != null) {
            eventService.updateEvent(id, eve);
            return new ResponseEntity<>(eventService.getEventById(id), HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Event> deleteTodo(@PathVariable("id") Long id) {
        Event problem = eventService.getEventById(id);

        if(problem != null) {
            eventService.deleteEvent(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
