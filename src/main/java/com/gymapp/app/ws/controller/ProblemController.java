package com.gymapp.app.ws.controller;

//models
import com.gymapp.app.ws.model.Problem;

//datasource
import com.gymapp.app.ws.repository.ProblemRepository;
import com.gymapp.app.ws.repository.TestData;
import com.gymapp.app.ws.serviceInterface.ProblemServiceInterface;
import com.gymapp.app.ws.services.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("problem")
@CrossOrigin
public class ProblemController {

    ProblemServiceInterface problemService;

    public ProblemController(ProblemServiceInterface problemService) {
        this.problemService = problemService;
    }

    @GetMapping
    public ResponseEntity<List<Problem>> getAllProblems() {
        List<Problem> problems = problemService.getProblems();
        return new ResponseEntity<>(problems, HttpStatus.OK);
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<Problem> getProblemById(@PathVariable Long id) {
        Problem problem = problemService.getProblemById(id);

        if(problem != null) {
            return new ResponseEntity<>(problem, HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping
    public ResponseEntity<Problem> saveProblem(@RequestBody Problem prob) {
        Problem problem = problemService.addProblem(prob);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("problem", "/problem");
        return new ResponseEntity<>(problem, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<Problem> updateTodo(@PathVariable("id") Long id, @RequestBody Problem prob) {
        Problem problem = problemService.getProblemById(id);

        if(problem != null) {
            problemService.updateProblem(id, prob);
            return new ResponseEntity<>(problemService.getProblemById(id), HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Problem> deleteTodo(@PathVariable("id") Long id) {
        Problem problem = problemService.getProblemById(id);

        if(problem != null) {
            problemService.deleteProblem(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
