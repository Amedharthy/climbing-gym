package com.gymapp.app.ws.controller;

import com.gymapp.app.ws.model.Account;
import com.gymapp.app.ws.model.Event;
import com.gymapp.app.ws.model.request.AccountCreateRequest;
import com.gymapp.app.ws.repository.AccountRepository;
import com.gymapp.app.ws.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public ResponseEntity<List<Account>> getAccounts() {
        List<Account> accounts = accountService.getAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createAccount (@RequestBody AccountCreateRequest accountCreateRequest) {

        accountService.createAccount(accountCreateRequest);
        return ResponseEntity.status(HttpStatus.OK).body(accountCreateRequest.getUsername() + accountCreateRequest.getPassword());
    }
}
