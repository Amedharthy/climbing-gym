package com.gymapp.app.ws.repository;

import com.gymapp.app.ws.model.Problem;
import com.gymapp.app.ws.model.Event;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestData {
    private final List<Problem> problemList = new ArrayList<>();
    private final List<Event> eventList = new ArrayList<>();

    public TestData() {/*
        problemList.add(new Problem(1,"V5",1));
        problemList.add(new Problem(2,"V3",2));
        problemList.add(new Problem(3,"V4",1));
*/

    }

    //Problems

    public List<Problem> getProblems() {
        return problemList;
    }
/*
    public Problem getProblem(int nr) {
        for (Problem problem : problemList) {
            if (problem.getId() == nr)
                return problem;
        }
        return null;
    }

    public boolean deleteProblem(int id) {
        Problem problem = getProblem(id);
        if (problem == null){
            return false;
        }
        return problemList.remove(problem);
    }


    public boolean addProblem(Problem problem) {
        if (this.getProblem(problem.getId()) != null){
            return false;
        }
        problemList.add(problem);
        return true;
    }

    public boolean updateProblem(Problem problem) {
        Problem old = this.getProblem(problem.getId());
        if (old == null) {
            return false;
        }
        old.setDifficulty(problem.getDifficulty());
        old.setFloor(problem.getFloor());
        return true;
    }
*/


    //Events

    public List<Event> getEventList() {
        return eventList;
    }


}
