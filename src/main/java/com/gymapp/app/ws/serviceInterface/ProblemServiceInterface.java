package com.gymapp.app.ws.serviceInterface;

import com.gymapp.app.ws.model.Problem;
import java.util.List;

public interface ProblemServiceInterface {
    List<Problem> getProblems();

    Problem getProblemById(Long id);

    Problem addProblem(Problem problem);

    void updateProblem(Long id, Problem problem);

    void deleteProblem(Long id);

}
