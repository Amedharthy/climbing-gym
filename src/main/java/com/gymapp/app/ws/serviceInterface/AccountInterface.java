package com.gymapp.app.ws.serviceInterface;

import com.gymapp.app.ws.model.Account;
import com.gymapp.app.ws.model.Problem;
import com.gymapp.app.ws.model.request.AccountCreateRequest;

import java.util.List;

public interface AccountInterface {

    Account readAccountByUsername(String id);

     Account createAccount(AccountCreateRequest accountCreateRequest);
}
