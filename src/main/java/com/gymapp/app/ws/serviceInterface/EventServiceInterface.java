package com.gymapp.app.ws.serviceInterface;

import com.gymapp.app.ws.model.Event;

import java.util.List;

public interface EventServiceInterface {

    List<Event> getEvents();

    Event getEventById(Long id);

    Event addEvent(Event event);

    void updateEvent(Long id, Event event);

    void deleteEvent(Long id);
}
